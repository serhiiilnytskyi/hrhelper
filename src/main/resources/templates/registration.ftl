<#import "parts/common.ftl" as c>
<@c.page>
Add new user
    <#if message??>${message}<#else></#if>
    <form action="registration" method="post">
        <div><label> User Name : <input type="text" name="username"/> </label></div>
        <div><label> Password: <input type="password" name="password"/> </label></div>
        <div>
            <select class="custom-select" name="departmentId">
                <#if departments??>
                    <#list departments as department>
                        <option value="${department.id}">${department.name}</option>
                    </#list>
                </#if>
            </select>
        </div>
        <div><input type="submit" value="Sign In"/></div>
    </form>
</@c.page>

<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>
<@c.page>
    <div>
        <@l.logout/>
        <span><a href="/user">user list</a></span>
    </div>
    <div>
        <form method="post" enctype="multipart/form-data">
            <input type="text" name="title" placeholder="Введіть заголовок">
            <input type="text" name="text" placeholder="Введіть повідомлення">
            <input type="text" name="tag" placeholder="Введіть тег">
            <input type="file" name="file">
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <button type="submit"> Добавити</button>

        </form>
    </div>
    <div>Список статей</div>

    <form method="get" action="/main">
        <input type="text" name="filter" value="<#if filter??>${filter}</#if>">
        <button type="submit">Знайти</button>
    </form>

    <#list pages as page>
        <div>
            <b>${page.id}</b>
            <span>${page.title}</span>
            <span>${page.body}</span>
            <i>${page.tag}</i>
            <strong>${page.authorName}</strong>
            <div>
                <#if page.filename??>
                    <img src="/img/${page.filename}" alt="">
                </#if>
            </div>
        </div>
    <#else>
        No pages
    </#list>
</@c.page>

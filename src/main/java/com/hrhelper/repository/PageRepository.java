package com.hrhelper.repository;

import com.hrhelper.model.Page;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PageRepository extends CrudRepository<Page, Integer> {

    List<Page> findByTag(String tag);
}

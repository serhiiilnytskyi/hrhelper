package com.hrhelper.controller;

import com.hrhelper.model.Department;
import com.hrhelper.model.Role;
import com.hrhelper.model.User;
import com.hrhelper.repository.DepartmentRepository;
import com.hrhelper.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Controller
public class RegistrationController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @GetMapping("/registration")
    public String registration(Map<String, Object> model) {
        List<Department> departments = departmentRepository.findAll();
        model.put("departments", departments);
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(User user,
                          @RequestParam int departmentId,
                          Map<String, Object> model) {
        User userFromDb = userRepository.findByUsername(user.getUsername());

        if (userFromDb != null) {
            model.put("message", "User exists");
            return "registration";
        } else {
            user.setActive(true);
            user.setRoles(Collections.singleton(Role.USER));
            user.setDepartment(departmentRepository.findById(departmentId).get());
            userRepository.save(user);
            return "redirect:/login";
        }
    }
}

package com.hrhelper.controller;

import com.hrhelper.model.Chapter;
import com.hrhelper.model.Page;
import com.hrhelper.model.User;
import com.hrhelper.repository.PageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

@Controller
public class MainController {

    @Autowired
    private PageRepository pageRepository;

    @Value("${upload.path}")
    private String uploadPath;

    @GetMapping("/")
    public String greeting(Map<String, Object> model) {
        return "login";
    }

    @GetMapping("/main")
    public  String main(@RequestParam(required = false, defaultValue = "") String filter,  Model model) {
        Iterable<Page> pages = pageRepository.findAll();

        if (filter != null && !filter.isEmpty()) {
            pages = pageRepository.findByTag(filter);
        }else {
            pages = pageRepository.findAll();
        }

        model.addAttribute("pages", pages);
        model.addAttribute("filter", filter);
        return "main";
    }

    @PostMapping("/main")
    public String add(
            @AuthenticationPrincipal User user,
            @RequestParam String title,
            @RequestParam String text,
            @RequestParam String tag, Map<String, Object> model,
            @RequestParam("file") MultipartFile file
    ) throws IOException {
        Page page = new Page(title, text, tag, user, new Chapter());

        if (file != null && !file.getOriginalFilename().isEmpty()) {
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()){
                uploadDir.mkdir();
            }

            String uuidFile = UUID.randomUUID().toString();
            String resultFileName = uuidFile + "." + file.getOriginalFilename();

            file.transferTo(new File(uploadPath + "/" + resultFileName));

            page.setFilename(resultFileName);
        }

        pageRepository.save(page);

        Iterable<Page> pages = pageRepository.findAll();

        model.put("pages", pages);

        return "main";
    }
}

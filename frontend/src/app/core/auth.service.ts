import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from "rxjs";

@Injectable()
export class AuthService {

  baseUrl: 'http://localhost:8080/';

  constructor(private http: HttpClient) {
  }

  attemptAuth(ussername: string, password: string): Observable<any> {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'});
    const credentials = {username:ussername, password:password};
    console.log('attempAuth ::');
    return this.http.post<any>('http://localhost:8080/api/auth/', credentials);
  }

}
